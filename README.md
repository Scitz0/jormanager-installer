## Installation script for JorManager, a manager for Cardano ITN jormungandr nodes

This is a tool for Cardano ITN pool operators to quickly deploy latest version of JorManager on a new system.

JorManager is created by Andrew Westberg and all credit to him for creating this wonderful and useful manager.
Go to [JorManager bitbucket site](https://bitbucket.org/muamw10/jormanager) to read more about it.

---

## Highlights

* Downloads latest version of JorManager.
* Downloads and installs OpenJDK 13.0.2 if needed.
* Downloads latest official version of jormungandr (repalce binary after script has completed if another version of jormungandr is preferred)
* Configures most settings in JorManager application.properties.
* Configures most settings in jormungandr node config files.
* Creates neccessary folders for log files, storage etc. 
* If selected, it will setup systemd and all configurations for it.

---

## Instructions

Follow below guide to download and execute installation script for JorManager.


1. Download script.

      ```
      wget https://bitbucket.org/scitz0/jormanager-installer/raw/master/install-jormanager.sh
      ```
      
2. Make it executable.

      ```
      chmod +x install-jormanager.sh
      ```
      
3. Run script with: (must be run as root or with sudo)

      ```
      ./install-jormanager.sh
      ```
      
4. Follow script wizard and let it do all the work for you :)

5. Make sure to **carefully read final notes** after script has finished. 

---

Telegram support channel: https://t.me/jormanager

---

If you found this JorManager installation script useful, please consider donating **MainNet** ADA to:

```
DdzFFzCqrht9isNpAdvVB2x2dr686nfKLvxXZVjGikQxWmd6kYv5LwqbwKmsBj3Jbpt3rm4fSAFRK1PCua7RQ9H77VtyvJm3wrHzrA6R
```
 
and/or delegate to my TestNet ITN Pool AHLNET (Ticker: AHL)

```
9aaf473932cc245ef85e13d02821c2be7b0219bb20c13e62eaf503503c2d4fc3
```

---

###### Release Notes
0.8

  * realpath replaced with readlink for path normalization

0.7

  * Added support for AdaStat.net (JorManager 0.3.4->)
  
  * New semi-automatic upgrade option with instructions for how to manually proceed 
  
0.6

  * Fix for use of relative path in installer

0.5

  * Updated for JorManager 0.2.11(jormungandr 0.8.17->)

0.4

  * tar issue not extracting to correct folder on some systems when running with sudo -i 
  
  * Wildcard in path should be outside of quotes

0.3

  * Indentation issue for local trusted peers
  
  * Ability to set starting port for p2p/rest
  
  * Fixed issue when using more than 10 nodes
  
  * Internal code cleanup and script output

0.2

  * Copy&Paste error for cluster stats file path  
    Credit to Jun-Sik Choi for finding this one.

0.1
  
  * Status Monitoring Setup
  
  * Easier multi JorManager setup (specify path to files & unique systemd filenames)
  
  * Script version management/log
  
  * Various code improvements
