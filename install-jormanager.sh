#!/bin/bash
#
# Install script for JorManager, a Cardano jormungandr pool manager by Andrew Westberg
# https://bitbucket.org/muamw10/jormanager
#
# Author: Ola Ahlman
#
#+---------+------------+------------------------------------------------------+
#| Version | Date       | Notes                                                |
#+---------+------------+------------------------------------------------------+
#| 0.1     | 2020-02-26 | * Status Monitoring Setup                            |
#|         |            | * Easier multi JorManager setup                      |
#|         |            | * Script version management/log                      |
#|         |            | * Various code improvements                          |
#+---------+------------+------------------------------------------------------+
#| 0.2     | 2020-02-27 | * Copy&Paste error for cluster stats file path       |
#+---------+------------+------------------------------------------------------+
#| 0.3     | 2020-03-01 | * Indentation issue for local trusted peers          |
#|         |            | * Ability to set starting port for p2p/rest          |
#|         |            | * Fixed issue when using more than 10 nodes          |
#|         |            | * Internal code cleanup and script output            |
#+---------+------------+------------------------------------------------------+
#| 0.4     | 2020-03-20 | * tar issue not extracting to correct folder on      |
#|         |            |   some systems when running with sudo -i             |
#|         |            | * Wildcard in path should be outside of quotes       |
#+---------+------------+------------------------------------------------------+
#| 0.5     | 2020-04-05 | * Updated for JorManager 0.2.11(jormungandr 0.8.17->)|
#+---------+------------+------------------------------------------------------+
#| 0.6     | 2020-04-19 | * Fix for use of relative path in installer          |
#+---------+------------+------------------------------------------------------+
#| 0.7     | 2020-04-26 | * Added support for Adastat.net (JorManager 0.3.4)   |
#|         |            | * New semi-automatic upgrade option with             |
#|         |            |   instructions for how to manually proceed           |
#+---------+------------+------------------------------------------------------+
#| 0.8     | 2020-05-12 | * realpath not working as expected,                  |
#|         |            |   replaced with readlink                             |
#+---------+------------+------------------------------------------------------+
#

if [[ "$@" == *-v* ]]; then
  printf "\nv0.8\n\n"
fi

if [[ "$EUID" -ne 0 ]]; then
  printf "\n## Sorry, you need to run script as root or with sudo -i bash -c $(readlink -m $0)\n\n"
  exit 1
fi

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

main() {
  
  printf "\nInstalling needed prerequisites if missing.\n"
  update_repository
  install_prerequisite "jq"
  install_prerequisite "wget"
  install_prerequisite "curl"
  install_prerequisite "tar"
  install_prerequisite "bzip2"
  install_prerequisite "gzip"
  install_prerequisite "openssl"
  install_prerequisite "sed"
  install_prerequisite "argon2"
  install_prerequisite "readlink"
  install_prerequisite "dig" # install last as we need sed

  # JAVA
  JAVA_PATH=$(command -v java)
  if [[ -z ${JAVA_PATH} ]]; then
    printf "\n## Could not find path to java executable, do you wish to install OpenJDK 13.0.2?\n"
    select yn in "Yes" "No"; do
      case ${yn} in
        Yes ) printf "\n## Downloading and installing OpenJDK 13.0.2\n\n"
              wget https://download.java.net/java/GA/jdk13.0.2/d4173c853231432d94f001e99d882ca7/8/GPL/openjdk-13.0.2_linux-x64_bin.tar.gz -P "${SCRIPTPATH}"
              tar zxf "${SCRIPTPATH}/openjdk-13.0.2_linux-x64_bin.tar.gz" -C "${SCRIPTPATH}/"
              # Cleanup after extracting jdk
              rm -f "${SCRIPTPATH}/openjdk-13.0.2_linux-x64_bin.tar.gz"
              mv "${SCRIPTPATH}/jdk-13.0.2" /opt/
              echo 'export JAVA_HOME=/opt/jdk-13.0.2' > /etc/profile.d/jdk13.0.2.sh
              echo 'export PATH="$PATH:/opt/jdk-13.0.2/bin"' >> /etc/profile.d/jdk13.0.2.sh
              source /etc/profile.d/jdk13.0.2.sh
              break;;
        No )  printf "\n## Unable to continue with installation, java required!\nInstall manually and add to path or let this script install it for you\n\n"; exit;;
      esac
    done
    # Lets retest that Java was successfully installed 
    if [[ -n $(command -v java) ]]; then
      JAVA_PATH=$(command -v java)
    else
      printf "\nERROR: Failed to install Java, check output and ask for help in telegram support channel!\nhttps://t.me/jormanager\n\n"
      exit 1
    fi
  else
    printf "\n## Found Java executabe at: ${JAVA_PATH}\n\n"
  fi

  # Download JorManager
  printf "## Downloading latest version of JorManager...\n\n"
  # Using wget as curl doesn't handle redirects
  JM_LATEST_URL=$(curl -s https://api.bitbucket.org/2.0/repositories/muamw10/jormanager/downloads | jq -r '.values[0].links.self.href')
  JM_FILENAME="${JM_LATEST_URL##*/}"
  JM_FILENAME_WO_EXT=$(basename ${JM_FILENAME} .tar.bz2)
  wget -q --show-progress ${JM_LATEST_URL} -P "${SCRIPTPATH}"
  printf "\n"
  read -p "## Where do you want to install JorManager? [${SCRIPTPATH}/JorManager]: " JM_PATH
  JM_PATH=$(readlink -m ${JM_PATH:-"${SCRIPTPATH}/JorManager"})
  
  JM_RELEASEFOLDER="${JM_PATH}/releases/${JM_FILENAME_WO_EXT}"
  # New install or upgrade?
  if [[ -d "${JM_PATH}" && $(ls -lR "${JM_PATH}"/*.jar 2> /dev/null | wc -l) > 0 ]]; then
    printf "\n## Old installation found, do you want to upgrade or reinstall over old installation?\n\n"
    select ur in "Upgrade" "Reinstall"; do
      case ${ur} in
        Upgrade ) if [[ -d "${JM_RELEASEFOLDER}" ]]; then
                rm -f "${SCRIPTPATH}/${JM_FILENAME}"
                printf "\nLatest JorManager release already downloaded, check ${JM_RELEASEFOLDER}\n\n"
                read -p "Press enter to continue"
              else
                printf "\n## Creating release folder: ${JM_RELEASEFOLDER}\n"
                mkdir -p "${JM_RELEASEFOLDER}"
                printf "## Extracting JorManager to release folder\n"
                tar jxf "${SCRIPTPATH}/${JM_FILENAME}" -C "${JM_RELEASEFOLDER}"
                chown -R --reference="${JM_PATH}" "${JM_PATH}/releases"
                rm -f "${SCRIPTPATH}/${JM_FILENAME}"
              fi
              printf "\n\n########################\n"
              printf "\nUpgrade instructions:\n\n"
              printf "*) Stop JorManager\n"
              printf "*) Overwrite JorManager jar file with new:\n"
              printf "   cp \"${JM_RELEASEFOLDER}/jormanager\"*.jar \"${JM_PATH}/jormanager.jar\"\n"
              printf "*) Check release notes if there are changes needed to configuration file (application.properties)\n"
              printf "*) Start JorManager\n\n"
              printf "In case you want to upgrade Jormungandr to latest version this is a manual process outside the scope of this installer\n"
              printf "*) Download latest version from https://github.com/input-output-hk/jormungandr/releases\n"
              printf "*) Replace jormungandr and jcli files in ${JM_PATH} folder\n"
              printf "*) Restart JorManager or kill nodes one by one and let JorManager restart them to make a staggered upgrade\n\n"
              exit;;
        Reinstall ) printf "## In case of reinstallation please make sure to first backup blocks.json and JorManager/Jormungandr configuration files\n\n"
              read -p "Press enter to continue or CTRL-C to exit script"
              break;;
      esac
    done
  fi 
  
  printf "\n## Creating folder(s): ${JM_PATH}\n\n"
  mkdir -p "${JM_PATH}"
  printf "Creating keystorage folder: ${JM_PATH}/keystorage\n"
  mkdir -p "${JM_PATH}/keystorage"
  printf "## Extracting JorManager\n\n"
  tar jxf "${SCRIPTPATH}/${JM_FILENAME}" -C "${JM_PATH}"
  printf "\n## Creating release folder for backup: ${JM_RELEASEFOLDER}\n\n"
  mkdir -p "${JM_RELEASEFOLDER}"
  printf "## Extracting JorManager to backup folder\n\n"
  tar jxf "${SCRIPTPATH}/${JM_FILENAME}" -C "${JM_RELEASEFOLDER}"
  
  # Cleanup after extracting JorManager
  rm -f "${SCRIPTPATH}/${JM_FILENAME}"
  mv "${JM_PATH}/jormanager"*.jar "${JM_PATH}/jormanager.jar"
  
  # Get number of nodes to run
  while true; do
    read -p "## How many jormungandr nodes do you want to run? [3]: " J_NODES
    J_NODES=${J_NODES:-3}
    if ! [[ "${J_NODES}" =~ ^[1-9][0-9]?$ ]]; then
      echo "\nERROR: Please input a number between 1-99!\n\n"
    else
      break
    fi
  done
  
  # Get external ip
  EXT_IP_RESOLVE=$(dig @resolver1.opendns.com ANY myip.opendns.com +short)
  printf "\n"
  read -p "## Enter your external IP [${EXT_IP_RESOLVE}]: " EXT_IP
  EXT_IP=${EXT_IP:-"${EXT_IP_RESOLVE}"}
  
  # P2P starting port
  printf "\n## Jormungandr P2P listen port"
  printf "\n- Specify prefix to use, it will automatically add pid with two digits at the end."
  printf "\n- <prefix><pid>\n- Example:\n- Prefix = 345, this would give 34500 as starting port."
  printf "\n- Valid port range for prefix [11-655].\n\n"
  while true; do
    read -p "What port prefix do you want to use for p2p listen/public address? [33]: " P2P_PORT_PREFIX
    P2P_PORT_PREFIX=${P2P_PORT_PREFIX:-33}
    if [[ "${P2P_PORT_PREFIX}" =~ ^[0-9]+$ ]] && [ "${P2P_PORT_PREFIX}" -ge 11 ] && [ "${P2P_PORT_PREFIX}" -le 655 ]; then
      break
    else
      printf "\nERROR: Please input a valid P2P port prefix [11-655]!\n\n"
    fi
  done
  
  # REST starting port
  printf "\n## Jormungandr REST port"
  printf "\n- Specify prefix to use, it will automatically add pid with two digits at the end."
  printf "\n- <prefix><pid>\n- Example:\n- Prefix = 90, this would give 9000 as starting port."
  printf "\n- Valid port range for prefix [11-655]."
  printf "\n- Port must be unique and must NOT be the same as for P2P.\n\n"
  while true; do
    read -p "What port prefix do you want to use for REST? [34]: " REST_PORT_PREFIX
    REST_PORT_PREFIX=${REST_PORT_PREFIX:-34}
    if [[ "${REST_PORT_PREFIX}" =~ ^[0-9]+$ ]] && [ "${REST_PORT_PREFIX}" -ge 11 ] && [ "${REST_PORT_PREFIX}" -le 655 ]; then
      if [[ "${REST_PORT_PREFIX}" -eq "${P2P_PORT_PREFIX}" ]]; then
        printf "\nERROR: REST and P2P listen port prefix can not be the same!\n\n"
      else
        break
      fi
    else
      printf "\nERROR: Please input a valid REST port prefix [11-655]!\n\n"
    fi
  done
  
  # Create jormungandr config files
  # Generate public id prefix to share between nodes
  PUBLIC_ID_PREFIX=$(openssl rand -hex 18)
  for ((i=0;i<${J_NODES};i++)); do
    NODE_PID=$(printf %02d ${i})
    CONFIG_FILE=${JM_PATH}/itn_rewards_v1-config${NODE_PID}.yaml
    # Create a copy of default config file
    cp ${JM_PATH}/itn_rewards_v1-configXX.yaml ${CONFIG_FILE}
    # Replace 33/34<pid> with modified port numbers
    sed -i 's|33<pid>|'"${P2P_PORT_PREFIX}${NODE_PID}"'|g' ${CONFIG_FILE}
    sed -i 's|34<pid>|'"${REST_PORT_PREFIX}${NODE_PID}"'|g' ${CONFIG_FILE}
    # Replace <public_ip> with correct external ip
    sed -i 's|<public_ip>|'"${EXT_IP}"'|g' ${CONFIG_FILE}
    # Replace public_id prefix with previously created prefix
    sed -i 's|<public_id_prefix>|'"${PUBLIC_ID_PREFIX}"'|g' ${CONFIG_FILE}
    # Replace public_id suffix with node id and zeros
    sed -i 's|<public_id_suffix>|'"${NODE_PID}${NODE_PID}${NODE_PID}"'000000|g' ${CONFIG_FILE}
    # Build variable with all local peers except self
    PEER_LIST='' # trusted_peers
    PREFERRED_LIST='' # preferred_list
    for ((j=0;j<${J_NODES};j++)); do
      if [[ "${j}" -ne "${i}" ]]; then
        PEER_PID=$(printf %02d ${j})
        PEER_LIST="${PEER_LIST}"'- address: "\/ip4\/127\.0\.0\.1\/tcp\/'"${P2P_PORT_PREFIX}${PEER_PID}"'"\n    id: "'"${PUBLIC_ID_PREFIX}${PEER_PID}${PEER_PID}${PEER_PID}"'000000"\n  '
        PREFERRED_LIST="${PREFERRED_LIST}"'- address: "\/ip4\/127\.0\.0\.1\/tcp\/'"${P2P_PORT_PREFIX}${PEER_PID}"'"\n          id: "'"${PUBLIC_ID_PREFIX}${PEER_PID}${PEER_PID}${PEER_PID}"'000000"\n        '
      fi
    done
    # Remove last newline
    PEER_LIST=$(echo "${PEER_LIST}" | sed 's/\(.*\)\\n.*/\1 /')
    PREFERRED_LIST=$(echo "${PREFERRED_LIST}" | sed 's/\(.*\)\\n.*/\1 /')
    # Replace <my_peers> with correct list
    sed -i 's|<my_peers>|'"${PEER_LIST}"'|g' ${CONFIG_FILE}
    # Replace <preferred_list> with correct list
    sed -i 's|<preferred_list>|'"${PREFERRED_LIST}"'|g' ${CONFIG_FILE}
  done
  # Remove default config files
  rm -f ${JM_PATH}/itn_rewards_v1-configXX.yaml

  # Update placeholders in application.properties
  # Some manual adjustments needs to be performed
  sed -i 's|33{pid}|'"${P2P_PORT_PREFIX}{pid}"'|g' ${JM_PATH}/application.properties
  sed -i 's|34{pid}|'"${REST_PORT_PREFIX}{pid}"'|g' ${JM_PATH}/application.properties
  sed -i 's|jormanager\.nodecount=.*|jormanager\.nodecount='"${J_NODES}"'|g' ${JM_PATH}/application.properties
  sed -i 's|<jormanager_path>|'"${JM_PATH}"'|g' ${JM_PATH}/application.properties
  sed -i 's|<public_ip>|'"${EXT_IP}"'|g' ${JM_PATH}/application.properties
  printf '\n%s\n%s\n%s' \
           "## In what mode do you want to start nodes?" \
           "Leader = Leadership election active and nodes ready to create blocks" \
           "Standby = Leadership election active but no node promoted to leader, ready for quick takeover" \
           "Passive = No leadership election performed"
  select lsp in "Leader" "Standby" "Passive"; do
    case ${lsp} in
      Leader )
                local NODE_PASSIVE_LIST=$(build_passive_list ${J_NODES} "false")
                sed -i 's|jormanager\.node\.passive=.*|jormanager\.node\.passive='"${NODE_PASSIVE_LIST}"'|g' ${JM_PATH}/application.properties
                break;;
      Passive ) 
                local NODE_PASSIVE_LIST=$(build_passive_list ${J_NODES} "true")
                sed -i 's|jormanager\.node\.passive=.*|jormanager\.node\.passive='"${NODE_PASSIVE_LIST}"'|g' ${JM_PATH}/application.properties
                break;;
      Standby )
                local NODE_PASSIVE_LIST=$(build_passive_list ${J_NODES} "false")
                sed -i 's|jormanager\.node\.passive=.*|jormanager\.node\.passive='"${NODE_PASSIVE_LIST}"'|g' ${JM_PATH}/application.properties
                sed -i 's|jormanager\.standby\.mode=.*|jormanager\.standby\.mode=true|g' ${JM_PATH}/application.properties
                break;;
    esac
  done

  # pooltool configuration in application.properties
  printf "\n## Do you want to send block height & leader block counts to pooltool.io? (recommended if set to Leader or Standby)\n"
  select yn in "Yes" "No"; do
    case ${yn} in
      Yes ) if [[ "${lsp}" == "Standby" ]]; then
              sed -i 's|jormanager\.pooltool\.enabled=.*|jormanager\.pooltool\.enabled=false|g' ${JM_PATH}/application.properties
            else
              sed -i 's|jormanager\.pooltool\.enabled=.*|jormanager\.pooltool\.enabled=true|g' ${JM_PATH}/application.properties
            fi
            while true; do
              read -p "## Enter pool id: " POOL_ID
              if [ ${#POOL_ID} -ne 64 ]; then
                printf "\nERROR: Invalid pool id!\nShould be 64 characters\n\n"
              else
                read -p "## Enter API ID for pooltool.io (https://pooltool.io/profile): " USER_ID
                if [ ${#USER_ID} -ne 36 ]; then
                  printf "\nERROR: Invalid pooltool API ID!\nShould match format deadbeef-dead-dead-dead-deadbeefdead\n\n"
                else
                  sed -i 's|<pooltool_poolId>|'"${POOL_ID}"'|g' ${JM_PATH}/application.properties
                  sed -i 's|<pooltool_userId>|'"${USER_ID}"'|g' ${JM_PATH}/application.properties
                  break
                fi
              fi
            done
            break;;
      No )  sed -i 's|jormanager\.pooltool\.enabled=.*|jormanager\.pooltool\.enabled=false|g' ${JM_PATH}/application.properties; break;;
    esac
  done
  
  # adastat configuration in application.properties
  printf "\n## Do you want to send leader block counts to AdaStat.net? (recommended if set to Leader or Standby)\n"
  select yn in "Yes" "No"; do
    case ${yn} in
      Yes ) if [[ "${lsp}" == "Standby" ]]; then
              sed -i 's|jormanager\.adastat\.enabled=.*|jormanager\.adastat\.enabled=false|g' ${JM_PATH}/application.properties
            else
              sed -i 's|jormanager\.adastat\.enabled=.*|jormanager\.adastat\.enabled=true|g' ${JM_PATH}/application.properties
            fi
            if [[ -z ${POOL_ID} ]]; then
              while true; do
                read -p "## Enter pool id: " POOL_ID
                if [ ${#POOL_ID} -ne 64 ]; then
                  printf "\nERROR: Invalid pool id!\nShould be 64 characters\n\n"
                else
                  break
                fi
              done
            fi
            sed -i 's|<pooltool_poolId>|'"${POOL_ID}"'|g' ${JM_PATH}/application.properties
            break;;
      No )  sed -i 's|jormanager\.adastat\.enabled=.*|jormanager\.adastat\.enabled=false|g' ${JM_PATH}/application.properties; break;;
    esac
  done
  
  # Status Monitoring Setup
  # Set server port
  while true; do
    printf "\n"
    read -p "## What port number do you want to use for status monitoring? [8080]: " SERVER_PORT
    SERVER_PORT=${SERVER_PORT:-8080}
    if [[ "${SERVER_PORT}" =~ ^[0-9]+$ ]] && [ "${SERVER_PORT}" -ge 1024 ] && [ "${SERVER_PORT}" -le 65535 ]; then
      break
    else
      echo "\nERROR: Please input a valid port number [1024-65535]!\n\n"
    fi
  done
  sed -i 's|server\.port=.*|server\.port='"${SERVER_PORT}"'|g' ${JM_PATH}/application.properties
  # Set admin username
  printf "\n"
  read -p "## Admin username [admin]: " ADMIN_USER
  ADMIN_USER=${ADMIN_USER:-"admin"}
  sed -i 's|jormanager\.admin\.username=.*|jormanager\.admin\.username='"${ADMIN_USER}"'|g' ${JM_PATH}/application.properties
  # Generate argon2 encoded password & secret
  while true; do
    printf "\n"
    read -p "## Enter Admin Password (length > 8): " ADMIN_PASSWORD
    if [ ${#ADMIN_PASSWORD} -lt 8 ]; then 
      printf "\nERROR: Password length too short, please use a minimum of 8 characters.\n\n"
      continue
    fi
    ARGON2_SALT=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
    ADMIN_ENC_PASSWORD1=$(echo -n "${ADMIN_PASSWORD}" | argon2 "${ARGON2_SALT}" -id | awk '/Encoded:/{print $2}')
    read -p "## Confirm Password: " PASSWORD_CHECK
    ADMIN_ENC_PASSWORD2=$(echo -n "${PASSWORD_CHECK}" | argon2 "${ARGON2_SALT}" -id | awk '/Encoded:/{print $2}')
    if [[ "${ADMIN_ENC_PASSWORD1}" == "${ADMIN_ENC_PASSWORD2}" ]]; then
      sed -i 's|jormanager\.admin\.password=.*|jormanager\.admin\.password='"${ADMIN_ENC_PASSWORD1}"'|g' ${JM_PATH}/application.properties
      RANDOM_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
      ARGON2_SALT=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
      JWT_SECRET=$(echo -n "${RANDOM_PASSWORD}" | argon2 "${ARGON2_SALT}" -id | awk '/Encoded:/{print $2}')
      sed -i 's|jormanager\.jwt\.secret=.*|jormanager\.jwt\.secret={argon2}'"${JWT_SECRET}"'|g' ${JM_PATH}/application.properties
      break      
    fi
    printf "\n## Password missmatch, please retype password!\n"
  done
  
  # Create storage folders and update application.properties to reflect change
  printf "\n## Creating storage folders:\n"
  for ((i=0;i<${J_NODES};i++)); do
    NODE_PID=$(printf %02d ${i})
    mkdir -p ${JM_PATH}/storage/${NODE_PID}
    printf "${JM_PATH}/storage/${NODE_PID}\n"
  done
  sed -i 's|storage{pid}|storage/{pid}|g' ${JM_PATH}/application.properties

  # Create logs folder and update application.properties to reflect change
  printf "\n## Creating logs folder\n"
  mkdir -p ${JM_PATH}/logs
  sed -i 's|jormungandr{pid}\.log|logs/jormungandr{pid}\.log|g' ${JM_PATH}/application.properties

  # Download latest official version of jormungandr
  printf "\n## Downloading and installing latest official version of jormungandr\n"
  printf "## Choose asset to download for your system\n\n"
  J_ASSETS_URL=($(curl -s https://api.github.com/repos/input-output-hk/jormungandr/releases/latest | jq -r '.assets[].browser_download_url'))
  J_ASSETS_NAME=($(curl -s https://api.github.com/repos/input-output-hk/jormungandr/releases/latest | jq -r '.assets[].name'))
  select J_ASSET in "${J_ASSETS_NAME[@]}"; do
    case ${J_ASSET} in
      ${J_ASSET})
                for i in "${!J_ASSETS_URL[@]}"; do
                  if [[ "${J_ASSETS_URL[${i}]}" == *"${J_ASSET}" ]]; then
                    J_ASSET_URL="${J_ASSETS_URL[${i}]}"
                    break
                  fi
                done
                if [[ -z ${J_ASSET_URL} ]]; then
                  printf "## ERROR: Failed to get URL for jormungandr asset to download!\n\n"
                  printf "## Exiting script, report error to devs.\n\n"
                  exit 1
                fi
                break;;
    esac
  done
  printf "\n## Downloading and extracting ${J_ASSET}\n\n"
  wget -q --show-progress ${J_ASSET_URL} -P "${SCRIPTPATH}"
  tar zxvf "${SCRIPTPATH}/${J_ASSET}" -C ${JM_PATH}
  rm -f "${SCRIPTPATH}/${J_ASSET}"
  for ((i=0;i<${J_NODES};i++)); do
    NODE_PID=$(printf %02d ${i})
    ln -s ${JM_PATH}/jormungandr ${JM_PATH}/jormungandr${NODE_PID}
    printf "## Creating symbolic link: ${JM_PATH}/jormungandr${NODE_PID} -> ${JM_PATH}/jormungandr\n"
  done

  # Create blocks.json file
  printf "\n## Creating empty historical block log file. This keeps track of blocks you've minted.\n"
  while true; do
    read -p "## Path to file [/var/www/html/blocks.json]: " BLOCKS_PATH
    BLOCKS_PATH=$(readlink -m ${BLOCKS_PATH:-"/var/www/html/blocks.json"})
    if [[ "${BLOCKS_PATH}" =~ ^.*\.json$ ]]; then
      mkdir -p $(dirname "${BLOCKS_PATH}")
      echo "[]" > "${BLOCKS_PATH}"
      sed -i 's|jormanager\.block_log=.*|jormanager\.block_log='"${BLOCKS_PATH}"'|g' ${JM_PATH}/application.properties
      break
    fi
    printf "\nERROR: Path does not end with .json, please specify full path to json file!\n\n"
  done

  # Create jormanager-stats.json file
  printf "\n## Creating empty cluster stats file. This can be used to drive dashboards.\n"
  while true; do
    read -p "## Path to file [/var/www/html/jormanager-stats.json]: " STATS_PATH
    STATS_PATH=$(readlink -m ${STATS_PATH:-"/var/www/html/jormanager-stats.json"})
    if [[ "${STATS_PATH}" =~ ^.*\.json$ ]]; then
      mkdir -p $(dirname "${STATS_PATH}")
      touch "${STATS_PATH}"
      sed -i 's|jormanager\.stats_log=.*|jormanager\.stats_log='"${STATS_PATH}"'|g' ${JM_PATH}/application.properties
      break
    fi
    printf "\nERROR: Path does not end with .json, please specify full path to json file!\n\n"
  done
  
  # Set permissions
  SYSTEM_USERS=(root $(eval getent passwd {$(awk '/^UID_MIN/ {print $2}' /etc/login.defs)..$(awk '/^UID_MAX/ {print $2}' /etc/login.defs)} | cut -d: -f1))
  printf "\n## Run JorManager as this user:\n"
  select OWNER in "${SYSTEM_USERS[@]}"; do
    case ${OWNER} in
      ${OWNER})
              chown -R ${OWNER}:${OWNER} ${JM_PATH}
              chown ${OWNER}:${OWNER} "${BLOCKS_PATH}"
              chown ${OWNER}:${OWNER} "${STATS_PATH}"
              break;;
    esac
  done

  # systemd/rsyslog setup
  printf "\n## Do you want to use systemd to control JorManager? (recommended)\n"
  select yn in "Yes" "No"; do
    case ${yn} in
      Yes ) printf "\nCopying and setting up systemd configuration files\n\n"
            read -p "## Give systemd installation a name [jormanager]: " SYSTEMD_NAME
            SYSTEMD_NAME=${SYSTEMD_NAME:-"jormanager"}
            cp ${JM_PATH}/jormanager.service "/etc/systemd/system/${SYSTEMD_NAME}.service"
            sed -i 's|<user>|'"${OWNER}"'|g' "/etc/systemd/system/${SYSTEMD_NAME}.service"
            sed -i 's|<jormanager_path>|'"${JM_PATH}"'|g' "/etc/systemd/system/${SYSTEMD_NAME}.service"
            sed -i 's|<java_exec>|'"${JAVA_PATH}"'|g' "/etc/systemd/system/${SYSTEMD_NAME}.service"
            sed -i 's|SyslogIdentifier=.*|'"SyslogIdentifier=${SYSTEMD_NAME}"'|g' "/etc/systemd/system/${SYSTEMD_NAME}.service"
            printf "Copying and setting up rsyslog for JorManager log file\n"
            cp ${JM_PATH}/00-jormanager.conf "/etc/rsyslog.d/00-${SYSTEMD_NAME}.conf"
            sed -i 's|jormanager|'"${SYSTEMD_NAME}"'|g' "/etc/rsyslog.d/00-${SYSTEMD_NAME}.conf"
            printf "\nCreating JorManager log file (/var/log/${SYSTEMD_NAME}.log)\n"
            touch "/var/log/${SYSTEMD_NAME}.log"
            chown syslog:adm "/var/log/${SYSTEMD_NAME}.log" > /dev/null 2>&1 # ignore errors if user/group doesn't exist
            printf "\nCreating symbolic link jormanager.log to ${SYSTEMD_NAME}.log in ${JM_PATH}/logs/\n"
            ln -s "/var/log/${SYSTEMD_NAME}.log" "${JM_PATH}/logs/jormanager.log"
            sleep 1
            printf "\nRestarting systemd to add new configuration\n"
            systemctl daemon-reload
            sleep 1
            printf "Restarting rsyslog to enable jormanager logging\n"
            systemctl restart rsyslog
            START_CMD="$ sudo systemctl start ${SYSTEMD_NAME}.service"
            break;;
      No )
            START_CMD="$ java -jar ${JM_PATH}/jormanager.jar"
            break;;
    esac
  done
  
  # Cleanup
  rm -f ${JM_PATH}/00-jormanager.conf
  rm -f ${JM_PATH}/jormanager.service

  # Start instructions
  printf "\n\n########################\n"
  printf "\nInstallation complete!\n\n"
  printf "Start with:\n"
  printf "$START_CMD\n\n"
  printf "[SYSTEMD] Watch the logs to monitor jormanager's operation:\n"
  printf "$ tail -f ${JM_PATH}/logs/jormanager.log\n\n"

  # Final words
  printf "Before starting JorManager make sure to go through these steps:\n"
  printf "*) If running as leader/standby, make sure to place poolsecret.yaml & poolsecret.json (needed in both formats) in ${JM_PATH} folder\n"
  printf "*) If adastat is enabled, place pool's kes private key(same as sigkey from poolsecret file) in a file called stakepool_kes.prv in ${JM_PATH} folder\n"
  printf "*) Open application.properties and verify that all settings and paths are correct\n"
  printf "*) Open jormungandr config files(itn_rewards_v1-configXX.yaml) and verify that all settings are correct\n"
  printf "*) Important! Don't forget to modify your firewall/router to allow these new ports from the outside. Ports: ${P2P_PORT_PREFIX}00-${P2P_PORT_PREFIX}$(printf %02d $(expr ${J_NODES} - 1))\n\n"
  printf "For more information about JorManger go to\nhttps://bitbucket.org/muamw10/jormanager\n"
  printf "Here you will find more information about JorManager and how to support Andrew to continue this work on JorManager by donating.\n\n"
  printf "Telegram support channel: https://t.me/jormanager\n\n"
  printf "As a final note, if you found this JorManager installation script useful, please consider donating MainNet ADA to:\n"
  printf "DdzFFzCqrht9isNpAdvVB2x2dr686nfKLvxXZVjGikQxWmd6kYv5LwqbwKmsBj3Jbpt3rm4fSAFRK1PCua7RQ9H77VtyvJm3wrHzrA6R\n"
  printf "or\nDelegate to my TestNet ITN Pool AHLNET (9aaf473932cc245ef85e13d02821c2be7b0219bb20c13e62eaf503503c2d4fc3).\n\n\n"
}

update_repository() {
  if [[ ! -z $(command -v apt) ]]; then
    apt -q -y update
  elif [[ ! -z $(command -v dnf) ]]; then
    dnf -q -y update
  elif [[ ! -z $(command -v yum) ]]; then
    yum -q -y update
  else
    printf "\n## Couldn't find any package manager on system."
    printf "\n## Lets try and continue anyway and hope all prerequisite apps are installed.\n\n"
    exit 1
  fi
}

install_prerequisite() {
  app=$1
  if [[ -z $(command -v ${app}) ]]; then
    # special case for dig, replace with correct package for debian vs redhat based distros
    if [[ ! -z $(command -v apt) ]]; then
      [[ "${app}" == "dig" ]] && app="dnsutils" # Handle special case
      apt -q -y install ${app}
    elif [[ ! -z $(command -v dnf) ]]; then
      [[ "${app}" == "dig" ]] && app="bind-utils" # Handle special case
      dnf -q -y install ${app}
    elif [[ ! -z $(command -v yum) ]]; then
      [[ "${app}" == "dig" ]] && app="bind-utils" # Handle special case
      yum -q -y install ${app}
    else
      printf "\n## Couldn't find any package manager on system."
      printf "\n## Install $1 prerequisite manually and relaunch script.\n\n"
      exit 1
    fi
    if [[ -z $(command -v $1) ]]; then
      printf "\n## Unable to install $1, either not available in repository or failure during installation." 
      printf "\n## Install $1 prerequisite manually and relaunch script.\n\n"
      exit 1
    else
      printf "\n## $1 installed.\n\n"
    fi
  fi
}

# Pass number of nodes and boolean as arguments to function
# Returns a list through echo, catch with $(build_passive_list)
build_passive_list() {
  NODE_PASSIVE_LIST="$2"
  for ((i=1;i<$1;i++)); do
    NODE_PASSIVE_LIST="${NODE_PASSIVE_LIST}, $2"
  done
  echo ${NODE_PASSIVE_LIST}
}

main